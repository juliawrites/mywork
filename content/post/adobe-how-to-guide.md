+++
author = "Julia Browne"
date = 2020-04-22
title = "Adobe Reader Markup Tools Guide"
series = "how-to guides"
css = "style.css"
+++

Click [here](https://drive.google.com/file/d/1T_sUd9rMMM1-VkV4-QYnUwyCXkoExjqH/view?usp=sharing) 
for a PDF version.

---

The **Adobe Reader** markup tools allow you to edit and leave comments on PDF documents. The tools are useful for collaboration between and within teams.

# Displaying the Toolbar

The toolbar makes it easy to use editing functions. When you first open Adobe Reader, the application displays the toolbar on the right-hand side of the screen. 
Alternatively, you can click the **Tools** tab to display the toolbar.

1. Select the **Comment** tool from either the **Tools** tab or directly from the right-hand toolbar.

![comment tools](/../images/comment_tools.png)

2. A **Comment** toolbar appears at the top of the open document.

![comment toolbar](/../images/comment_toolbar.png)

# Using Text Edit Tools

The **Text Edit Tools** function is a collection of markup tools that allows you to edit and leave comments on documents. 
To access these tools, highlight the text you want to mark up on your document then select the tool you want to use from the top toolbar.

![text edit tools](/../images/highlighted_text.png)

You can also access other markup tools by right-clicking the highlighted text and selecting a tool from the menu that appears.

## Adding comments to the text

### Add sticky note

Use the **Add sticky note** tool to add general comments or instructions to the document.

1. Either press **Ctrl + 6** keys or select **Add sticky note** from the top toolbar.

![add sticky note](/../images/add_sticky_note.png)

2. Type in the text in the comment box on the left-hand side of the screen and click **Post**.

![add sticky note example](/../images/add_sticky_note_example.png)

3. You can move the sticky note around the document by dragging and dropping it.

### Add note to replace selected text

Use the **Add note to replace selected text** tool to identify any section of text that needs to be replaced with new text in the document.

1. Highlight the text you want to replace.
2. Select **Add note to replace selected text** from the top toolbar. This draws a line through the selected text and opens a comment box on the right-hand side.

![replace text](/../images/replace_text.png)

3. Type your comment and click **Post**. Your comment now appears whenever the reader clicks on the affected part of the document. You can change the colour of the strikethrough by clicking the circle at the top toolbar.

![underline text](/../images/underline_text_example.png)

### Add text comment

Use the **Add text comment** tool to add new text directly onto the document.

1. Select **Add text comment** from the top toolbar. Click anywhere on the document to display a caret and a toolbar with font properties.
2. Type the text to be added to the document. The text appears where you typed it. You can move the text around by dragging and dropping it.

### Add text box

Use the **Add text box** tool to add a text box directly onto the document.

1. Select **Add text box** from the top toolbar. Click anywhere on the document to display a bordered text box and a toolbar with font properties.

![add text comment](/../images/add_text_comment.png)

2. Type the text to be displayed in the text box. The text appears where you typed it. You can move the text box around by dragging and dropping it.

![add text box](/../images/add_text_box_example.png)

## Editing the text

### Highlight text

Use the **Highlight text** tool to draw attention to a section of the document.

1. Highlight the text you want to highlight. 
2. Select **Highlight text** from the top toolbar. You can change the colour of the highlight by clicking the circle at the top toolbar.

![highlight text](/../images/highlight_text.png)

![highlight text example](/../images/highlight_text_example.png)

3. You can add a comment to the highlighted text by using the comment box on the left and clicking **Post**.

### Underline text

Use the **Underline text** tool to underline a section of text.

1. Highlight the text you want to underline.
2. Select **Underline text** from the top toolbar. You can change the colour of the underline by clicking the circle at the top toolbar.

![underline text](/../images/underline_text.png)

![underline text example](/../images/underline_text_example.png)

### Strikethrough text

Use the **Strikethrough text** tool to identify any section of text that needs to be removed from the document.

1. Highlight the text you want to remove.
2. Either press the **Backspace** key while the text is highlighted or select **Strikethrough text** from the top toolbar. 
This draws a line through the previously highlighted text.

![strikethrough text](/../images/strikethrough_text.png)

![strikethrough text example](/../images/strikethrough_text_example.png)

### Insert text at cursor

Use the **Insert text at cursor** tool to show that new text needs to be inserted into existing text.

1. Place the cursor on the location you want to add new text to.
2. Begin typing the text to be inserted or select **Insert text at cursor** from the top toolbar.

![replace text](/../images/replace_text.png)

3. Type the text that you want to insert and click **Post**. A small caret shows the location of the inserted text. 
You can change the colour of the caret by clicking the circle at the top toolbar.

![insert text example](/../images/insert_text_example.png)

# Using Drawing Tools

## Drawing on the text

### Drawing tools

Use the **Drawing tools** tool to add shapes, lines and text callouts to the document.

1. Select **Drawing tools** from the top toolbar. Select the drawing tool you want to use.

![drawing tools](/../images/use_drawing_tools.png)

2. Place it on the document. You can expand and minimize the shapes and the callout by clicking and dragging it. 
You can also move it around by dragging and dropping it.

![drawing tools example](/../images/drawing_tools_example.png)

### Erase drawing

Use the **Erase drawing** tool to erase drawings created by the **Drawing tools**. 

1. Select **Erase drawing** from the top toolbar.

![erase drawing](/../images/erase_drawing.png)

2. Click and drag the cursor over the page drawings to erase them.

![erase drawing example](/../images/erase_drawing_example.gif)

## Using stamps on the text

### Add stamp

Use the **Add stamp** tool to add stamps to the document and to create custom stamps. This feature allows you to mark documents as Approved, Confidential and Void, among others. 
It also allows you to emphasize sections that need a signature, and to stamp documents with your name and the current date.

1. Select **Add stamp** from the top toolbar.

![add stamp](/../images/add_stamp.png)

2. Select one of the options under **Dynamic**, **Sign Here** or **Standard Business**. 
Place the stamp on the document. You can move it around by dragging and dropping it.

![add stamp example](/../images/add_stamp_example.png)

# Using Additional Tools

## Adding attachments to the text

### Add a new attachment

Use the **Add a new attachment** tool to attach **files** or **audio** to the document.

#### Attach File

1. Select **Add a new attachment** from the top bar, then select **Attach File**. You can attach pictures, Word documents and PDFs.

![add attachment](/../images/add_attachment.png)

2. Click anywhere on the document to open your local files in a new window.
3. Select the file you want to attach and click **OK**. The **File Attachment Properties** window appears.

![attach file](/../images/attach_file_properties.png)

4. Select an **Icon** to represent your attachment, along with the colour of the icon and its opacity, and click **OK**. 
The selected icon appears on the document. You can move it around by dragging and dropping it.

![attach file example](/../images/attach_file_example.png)

#### Record Audio

1. Select **Add new attachment** from the top bar, then select **Record Audio**. 
You can attach recordings of personal commentary and company meetings.

![add attachment](/../images/add_attachment.png)

2. Click anywhere on the document to open a **Sound Recorder** window.

![record audio](/../images/record_audio.png)

3. Record a new audio file or browse for an existing audio file and click **OK**. The **Sound Attachment Properties** window appears.

![record audio properties](/../images/record_audio_properties.png)

4. Select an **Icon** to represent your attachment, along with the colour of the icon and its opacity, and click **OK**. 
The selected icon appears on the document. You can move it around by dragging and dropping it.

![attach audio](/../images/attach_audio_example.png)

# Responding to Comments and Resolving Markup Changes

## Responding to Comments

Use the **Comment box** to perform comment-related actions. The **Comment box** is on the right-hand side of the screen.

![comment box](/../images/comment.png)

### Post a Comment

1. Click on the **comment** you want to reply to. An **"Add a reply..."** section appears under the comment.

![add a reply](/../images/add_a_reply.png)

2. Enter a comment and click **Post**. Your reply to the original comment appears under the original comment.

![comment reply](/../images/comment_reply.png)

3. Add another reply to begin a conversation thread. This facilitates communication between users and eliminates the 
inconvenience of having to exit the application to communicate through other means.

![comment reply reply](/../images/comment_reply_reply.png)

### Edit a Comment

1. Click the **ellipsis** to the right of the comment you want to edit.

![edit comment](/../images/edit_comment.png)

2. Select **Edit**.
3. Edit the text and click **Post**. The new text replaces the original text.

### Set a Comment Status

Use the **Set Status** option to easily add a status to a comment. 
The options are **Accepted**, **Rejected**, **Completed**, **Cancelled** and **None**.

1. Click the **ellipsis** to the right of the comment you want to set a status to.

![edit comment](/../images/edit_comment.png)

2. Select **Set Status** and choose one of the options. The status displays under the comment.

![comment status](/../images/comment_status.png)

3. To edit the status of an existing comment, select **Set Status** and choose the new status. The new status replaces the old status.

![edited comment status](/../images/comment_status_edited.png)

### Deleting a Comment

1. Click the **ellipsis** to the right of the comment you want to delete.

![edit comment](/../images/edit_comment.png)

2. Select **Delete**. The comment is removed.
3. To delete a comment thread, delete the parent comment. The entire thread is removed.

## Resolving Markup Changes and Comments

Use the **Delete** option to resolve any changes made or comments left by the markup tools described in the sections 
Using Text Edit Tools, Using Drawing Tools and Using Additional Tools.

### Resolve a Single Change or Comment

1. Right-click the change or comment on the PDF.
2. Select **Delete**. The change or comment is removed.

### Resolve Multiple Changes or Comments

1. To delete multiple changes and/or comments, press the **Ctrl** key and click the changes and/or comments you want to remove.
2. Right-click one of the selected changes and/or comments.
3. Select **Delete**. The multiple changes and/or comments are removed.
